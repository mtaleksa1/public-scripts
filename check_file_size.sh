#!/bin/bash

# IF NEEDED, INSTALL/UPDATE HOMEBREW PACKAGE MANAGER
if hash brew 2>/dev/null; then
echo "Homebrew already installed."
else
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# IF NEEDED, INSTALL COREUTILS (LINUX BASH COMMANDS -- I.E. du is gdu, sort is gsort)
brew install coreutils

# WRITE TWO FILES TO DESKTOP - ONE LOOKING FOR LARGE INDIVIDUAL FILES, AND ONE LOOKING FOR LARGE DIRS
sudo gdu --separate-dirs -ah | gsort -hr  | head -200 > ~/Desktop/GDU_NODIRS.txt
sudo gdu -ah | gsort -hr  | head -200 > ~/Desktop/GDU_WDIRS.txt

exit
